import {slowAddPromise} from "./stdlib";

async function main() {
    console.log("start");
    const a = await slowAddPromise(2, 3);
    console.log("this is blocked");
    console.log(a);
}


main();



