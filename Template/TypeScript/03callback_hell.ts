import {slowAdd} from "./stdlib";

const original = 5;
const b1 = 6;
const b2 = 6;
const b3 = 6;
const b4 = 6;
const b5 = 6;
const b6 = 6;

slowAdd(original, b1, (a1) => {
    console.log(a1);
    slowAdd(a1, b2, (a2) => {
        console.log(a2);
        slowAdd(a2, b3, (a3) => {
            console.log(a3);
            slowAdd(a3, b4, (a4) => {
                console.log(a4);
                slowAdd(a4, b5, (a5) => {
                    console.log(a5);
                    slowAdd(a5, b6, (a6) => {
                        console.log(a6);
                    });
                });
            });
        });
    });
});