import {slowAddPromise} from "./stdlib";

const original = 5;
const b1 = 6;
const b2 = 6;
const b3 = 6;
const b4 = 6;
const b5 = 6;
const b6 = 6;

async function main() {
    console.log("start");
    const a1 = await slowAddPromise(original, b1);
    const a2 = await slowAddPromise(a1, b2);
    const a3 = await slowAddPromise(a2, b3);
    const a4 = await slowAddPromise(a3, b4);
    const a5 = await slowAddPromise(a4, b5);
    const a6 = await slowAddPromise(a5, b6);
    console.log(a6);
}


main();
