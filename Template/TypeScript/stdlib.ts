const wait = (a: number, b: () => void) => setTimeout(b, a);

function slowAdd(a: number, b: number, callback: (c: number) => void): void {
    wait(2000, () => {
        callback(a + b);
    });
}

async function slowAddPromise(a: number, b: number): Promise<number> {
    return new Promise<number>((resolve) => {
        slowAdd(a, b, (ans) => {
            resolve(ans);
        });
    });
}

export {
    wait,
    slowAdd,
    slowAddPromise
}