
import {slowAddPromise} from "./stdlib";

const original = 5;
const b1 = 6;
const b2 = 6;
const b3 = 6;
const b4 = 6;
const b5 = 6;
const b6 = 6;

async function main() {
    console.log("start");
    const part1 = slowAddPromise(original, b1);
    const part2 = slowAddPromise(b2, b3);
    const part3 = slowAddPromise(b4, b5);
    const [a11, a12, a13] = await Promise.all([part1, part2, part3]);

    const part4 = slowAddPromise(a11, a12);
    const part5 = slowAddPromise(a13, b6);
    const [a1, a2] = await Promise.all([part4, part5]);

    const final = await slowAddPromise(a1,a2);
    console.log(final);

}


main();



