import {slowAddPromise} from "./stdlib";

const original = 5;
const b1 = 6;
const b2 = 6;
const b3 = 6;
const b4 = 6;
const b5 = 6;
const b6 = 6;

slowAddPromise(original, b1)
    .then(a1 => a1 + b2)
    .then(a2 => a2 + b3)
    .then(a3 => a3 + b4)
    .then(a4 => a4 + b5)
    .then(a5 => a5 + b6)
    .then(a6 => console.log(a6));
