using System;
using System.Reflection;
using System.Threading;

namespace C_
{
    public class Three
    {
        public static void Call()
        {
            Console.WriteLine("Hello World!");
            int a = 0, b = 1, c = 0, d = 2;
            var output = "";
            var t1 = new Thread(() =>
            {
                lock (output)
                {
                    output += Bad.SlowAdd(a, b).ToString();
                    Console.WriteLine("Finish Thread 1");
                }
            });
            var t2 = new Thread(() =>
            {
                lock (output)
                {
                    output += Bad.SlowAdd(c, d).ToString();
                    Console.WriteLine("Finish Thread 2");
                }
            });

            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();

            Console.WriteLine(output);
        }
    }
}