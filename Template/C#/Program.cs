﻿using System;
using System.Threading;

namespace C_
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(args[0]);

            switch (a)
            {
                case 0:
                    Zero.Call();
                    break;
                case 1:
                    One.Call();
                    break;
                case 2:
                    Two.Call();
                    break;
                case 3:
                    Three.Call();
                    break;
                case 4:
                    Four.Call();
                    break;
                case 5:
                    Five.Call();
                    break;
                case 6:
                    Six.Call().GetAwaiter().GetResult();
                    break;
                default: return;
            }
        }
    }
}