using System;
using System.Reflection;
using System.Threading;

namespace C_
{
    public class Two
    {
        public static void Call()
        {
            Console.WriteLine("Hello World!");
            int a = 0, b = 1, c = 0, d = 2;
            var output = "";
            var t1 = new Thread(() =>
            {
                output += Bad.SlowAdd(a, b).ToString();
            });
            var t2 = new Thread(() =>
            {
                output += Bad.SlowAdd(c, d).ToString();
            });

            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();

            Console.WriteLine(output);
        }
    }
}