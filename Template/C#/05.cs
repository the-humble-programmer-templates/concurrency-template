using System;
using System.Reflection;
using System.Threading;

namespace C_
{
    public class Five
    {
        public static void Call()
        {
            Console.WriteLine("Hello World!");
            var a = new object();
            var b = new object();
            var t1 = new Thread(() =>
            {
                lock (a)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Action 1 From Thread 1");
                    lock (b)
                    {
                        Console.WriteLine("Action 2 From Thread 1");
                    }
                }
            });
            var t2 = new Thread(() =>
            {
                lock (b)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Action 1 From Thread 2");
                    lock (a)
                    {
                        Console.WriteLine("Action 2 From Thread 2");
                    }
                }
            });

            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();
            Console.WriteLine("End");
        }
    }
}