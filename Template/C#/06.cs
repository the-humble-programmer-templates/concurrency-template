using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Medallion.Shell;

namespace C_
{
    public class Six

    {
        public async static Task Call()
        {
            var command = await Command.Run("git", "--version").Task;
            var output = command.StandardOutput;
            Console.WriteLine("Git version: " + output);
        }
    }
}