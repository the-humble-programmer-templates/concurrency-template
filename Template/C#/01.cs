using System;
using System.Reflection;

namespace C_
{
    public class One
    {
        public static void Call()
        {
            Console.WriteLine("Hello World!");
            int a = 0, b = 1, c = 0, d = 2;

            var output = "";
            output += Bad.SlowAdd(a, b).ToString();
            output += Bad.SlowAdd(c, d).ToString();
            Console.WriteLine(output);
        }
    }
}