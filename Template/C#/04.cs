using System;
using System.Reflection;
using System.Threading;

namespace C_
{
    public class Four
    {
        public static void Call()
        {
            Console.WriteLine("Hello World!");
            int a = 0, b = 1, c = 0, d = 2;
            var output = "";
            var t1 = new Thread(() =>
            {
                var ab = Bad.SlowAdd(a, b).ToString();
                lock (output)
                {
                    output += ab;
                    Console.WriteLine("Finish Thread 1");
                }
            });
            var t2 = new Thread(() =>
            {
                var cd =  Bad.SlowAdd(c, d).ToString();
                lock (output)
                {
                    output += cd;
                    Console.WriteLine("Finish Thread 2");
                }
            });

            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();

            Console.WriteLine(output);
        }
    }
}